# Latihan mandiri NodeJs

Tugas untuk kegiatan mandiri pertemuan 10 materi NodeJs

## Author : Efrayim Frydwin Nelson

## Installation

Project ini membutuhkan nodejs dan express. Untuk bisa menjalankan Project ini lakukan instalasi berikut:

```bash
npm init
```
```bash
npm install --save express
```
```bash
npm install --save hbs
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)